#ifndef BLUETOOTH_H
#define BLUETOOTH_H
#include "gamecontroller.h"
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothLocalDevice>
#include <QBluetoothServer>
#include <QDebug>
#include <QList>
#include <QObject>
#include <QTimer>
#include <QtBluetooth/qlowenergyadvertisingdata.h>
#include <QtBluetooth/qlowenergyadvertisingparameters.h>
#include <QtBluetooth/qlowenergycharacteristic.h>
#include <QtBluetooth/qlowenergycharacteristicdata.h>
#include <QtBluetooth/qlowenergycontroller.h>
#include <QtBluetooth/qlowenergydescriptordata.h>
#include <QtBluetooth/qlowenergyservice.h>
#include <QtBluetooth/qlowenergyservicedata.h>
#include <memory>
#include <mutex>
#include <qbluetooth.h>
#include <thread>
class Bluetooth : public QObject
{
    Q_OBJECT

  public:
    Bluetooth(GameController &controler);
    virtual ~Bluetooth() = default;
    Q_INVOKABLE void startDeviceSearch() const;
    Q_INVOKABLE void stopDeviceSearch() const;
    Q_INVOKABLE void setName(const QString &newName);
    Q_INVOKABLE QString getServerName() const;
    Q_INVOKABLE void startListen();
    Q_INVOKABLE void stopListen();

  public slots:
    void deviceFound(const QBluetoothDeviceInfo &device);
    void newConnection();
    void clientDisconnected();
    void readSocket();
    void printDeviceNames();

  signals:
    void messageReceivedViaBluetooth(const QString &sender, const QByteArray &message);
    void clientConnected(const QString &name);
    void clientDisconnectedSig(const QString &name);

  private:
    void initAdvertisingData();
    void initServiceData();
    /*Bluetooth*/
    std::shared_ptr<QBluetoothDeviceDiscoveryAgent> deviceExplorer;
    QList<QString> foundDeviceNames;
    std::shared_ptr<QBluetoothServer> commServer = nullptr;
    QBluetoothServiceInfo serviceInfo;
    QList<QBluetoothSocket *> clientSockets;
    std::mutex mtx;
    std::shared_ptr<QTimer> timerDebug;
    QString serverName = "STARSHIP BATTLE GAME";
    QLatin1String uuid;
    QBluetoothLocalDevice localDevice;
    /* Low energy */
    QLowEnergyAdvertisingData advertisingData;
    QLowEnergyCharacteristicData charData;
    QLowEnergyServiceData serviceData;
    QBluetoothSocket *socket = nullptr;
    GameController &gameControler;
    /*other depends*/
};

#endif // BLUETOOTH_H
