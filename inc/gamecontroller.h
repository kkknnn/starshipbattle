#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H
#include <QDebug>
#include <QObject>
class Bluetooth;

class GameController : public QObject
{
    friend class Bluetooth;
    enum ControlsKeys
    {
        Up = 0,
        Down,
        Right,
        Left,
        Triangle,
        Squere,
        Circle,
        Cross

    };

    Q_OBJECT
  public:
    explicit GameController(QObject *parent = nullptr);
    virtual ~GameController() = default;
    bool procedKey(GameController::ControlsKeys key);
  signals:

  public slots:
  private:
};

#endif // GAMECONTROLLER_H
