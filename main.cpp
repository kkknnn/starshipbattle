#include "bluetooth.h"
#include "gamecontroller.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{

    QQuickStyle::setStyle("Material");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    GameController gameController;
    Bluetooth bluetoothdriver(gameController);
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("cBluetooth", &bluetoothdriver);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
