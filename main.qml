import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.0
import QtQuick.Window 2.2
import QtQuick.Particles 2.12
import "qml" as ExternalComponents
import QtQuick.Scene3D 2.0


ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Blue game")
    Material.theme: Material.Dark
    Material.accent: Material.DeepOrange
    Material.primary: Material.Orange
    Material.foreground: "#eda618"

    Rectangle
    {
        id: background
        anchors.fill: parent
        color: "black"
        ExternalComponents.Particles
        {

            anchors.fill: parent
        }
    }

    Rectangle
    {
        id: footer
        height: parent.height*0.03
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: "#0a2f40"
        Label
        {
            anchors.centerIn: parent
            text: "Created by K.Nogaj"
            font.pointSize: parent.height*0.5
            color: "white"
            opacity: 0.6
        }


    }

    Rectangle
    {
        id: mainMenu
        property int roundness: 0
        property real scaleness: 1.0

        anchors.fill: parent
        anchors.rightMargin: 0.2 * parent.width
        anchors.leftMargin: 0.2 * parent.width
        anchors.topMargin: 0.3 * parent.height
        anchors.bottomMargin: 0.1 * parent.height
        color: "#e3b200"
        scale: scaleness
        radius: roundness
        opacity: 0.5
    }
        ListView
        {
            id: listMainMenu
            focus: true
            currentIndex: 0
            anchors.fill: mainMenu
            anchors.topMargin: 30
            scale: mainMenu.scaleness *0.9
            opacity: 1.0
            anchors.horizontalCenter: parent.horizontalCenter
            property int menuLevel: 0
            delegate:
                Rectangle {
                    id: itemDelegate
                    width: parent.width
                    height: 30
                    color: "#1b1b1b"
                    radius: 2
                    clip: true

                    Rectangle {
                      width: parent.width
                      height: parent.height
                      anchors.left: parent.left
                      color: "#f22613"
                      visible: itemDelegate.ListView.isCurrentItem ? true : false
                      opacity: 0.1
                      radius: 3
                    }

                    Text {
                        id: itexItem
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.leftMargin: 5
                        anchors.verticalCenter: parent.verticalCenter
                        font.pixelSize: 15
                        text: model.title
                        color: "white"
                        opacity: 1
                    }

                    Rectangle {
                      width: parent.width * 0.01
                      height: parent.height
                      anchors.left: parent.left
                      color: "#f22613"
                      visible: itemDelegate.ListView.isCurrentItem ? true : false
                    }
                    Rectangle {
                      width: parent.width * 0.01
                      height: parent.height
                      anchors.right: parent.right
                      color: "#f22613"
                      visible: itemDelegate.ListView.isCurrentItem ? true : false
                    }
                }

            model: mainMenuItems
            ListModel {
                           id: mainMenuItems
                           ListElement {
                               title: "NEW GAME"
                           }
                           ListElement {
                               title: "OPTIONS"

                           }
                           ListElement {
                               title: "INFORMATIONS"

                           }
                           ListElement {
                               title: "QUIT"

                           }

                       }
            header: Rectangle
            {
                opacity: 1.0
                Label
                {
                    anchors.centerIn: parent
                    text: "MENU"
                    background: Rectangle
                    {
                        color: "orange"
                    }

                    color: "white"
                }
            }



        SequentialAnimation
        {
                       loops: Animation.Infinite
                       running: true
                       NumberAnimation {
                           target: mainMenu
                           property: "scaleness"
                           duration: 20000
                           easing.type: Easing.InOutQuad
                           from: 1.0
                           to: 1.2

                       }
                       NumberAnimation {
                           target: mainMenu
                           property: "scaleness"
                           duration: 20000
                           easing.type: Easing.InOutQuad
                           from: 1.2
                           to: 1.0

                       }
                   }
        SequentialAnimation
        {
                       loops: Animation.Infinite
                       running: true
                       NumberAnimation {
                           target: mainMenu
                           property: "roundness"
                           duration: 5000
                           easing.type: Easing.InOutQuad
                           from: 10
                           to: 20

                       }
                       NumberAnimation {
                           target: mainMenu
                           property: "roundness"
                           duration: 5000
                           easing.type: Easing.InOutQuad
                           from: 20
                           to: 10

                       }
                   }



    }
    Rectangle
    {
    id:textPlace
    height: parent.height*0.2
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top
    color:"orange"


    Scene3D {
        id: scene3d
        anchors.fill: parent
        anchors.margins: 2
        focus: false
        antialiasing: true
        aspects: ["input", "logic"]
        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio
        ExternalComponents.StarShipBattle3dText
        {

        }

    }
    }


    Button
    {   id: buttonSearchBluetoothDevices
        property string textButton: "START"
        text: textButton

        onClicked:
        {
            if(textButton == "START")
            {
                cBluetooth.startListen()
                textButton = "STOP"
            }
            else
            {
                cBluetooth.stopDeviceSearch()
                textButton = "START"
            }
        }
    }
}
