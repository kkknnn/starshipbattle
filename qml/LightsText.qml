import QtQuick 2.1
import QtQuick.Controls 2.5
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0
import QtQuick.Controls.Material 2.3


Entity {
    id: lights
    property bool animationRunning: false

    Entity {
        components: [
            PointLight {
                color: "white"
                intensity: 1.5
                constantAttenuation: 1.0
                linearAttenuation: 0.0
                quadraticAttenuation: 0.0025


            },
            Transform {
                id: pointLightTransform
                property real xVal: 5.0
                translation: Qt.vector3d(xVal, 20.0, 40.0)
            }
        ]
        SequentialAnimation
        {
            running: animationRunning
            loops: Animation.Infinite

            NumberAnimation {
                target: pointLightTransform
                property: "xVal"
                duration: 10000
                easing.type: Easing.OutInBounce
                from: 0.0
                to: 20.0

            }
            NumberAnimation {
                target: pointLightTransform
                property: "xVal"
                duration: 10000
                easing.type: Easing.OutInBounce
                from: 20.0
                to: 0.0

            }
            NumberAnimation {
                target: pointLightTransform
                property: "xVal"
                duration: 10000
                easing.type: Easing.OutInBounce
                from: 0.0
                to: -20.0

            }
            NumberAnimation {
                target: pointLightTransform
                property: "xVal"
                duration: 10000
                easing.type: Easing.OutInBounce
                from: -20.0
                to: -0.0

            }

        }


    }
    Entity {
        components: [
            DirectionalLight {
                worldDirection: Qt.vector3d(pointLightTransform.xVal, -12.0, 0.0).normalized();
                color: "#fbf9ce"
                intensity: 0.2
            },
            Transform {
                        translation: Qt.vector3d(0.0, 20.0, 20.0)
            }
        ]
    }

    Entity {
        components: [
            DirectionalLight {
                worldDirection: Qt.vector3d(pointLightTransform.xVal, -10.0, 5.0).normalized();
                color: "orange"
                intensity: 0.15
            },
                      Transform {
                          id: dirLightTransform
                          translation: Qt.vector3d(0.0, 30.0, 20.0)

                          }
        ]
    }

    Entity {
        components: [
            SpotLight {
                localDirection: Qt.vector3d(0.0, -50.0, 0.0)
                color: "white"
                intensity: 0.15
            },
            Transform {
                id: spotLightTransform
                translation: Qt.vector3d(0.0, 100.0, 20.0)
                }

        ]
    }
}
