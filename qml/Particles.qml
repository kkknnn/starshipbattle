import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Particles 2.12
import QtQuick.Controls.Material 2.3

Item {
    id: containerParticles
    function randomNumber() {
            return Math.random() * 360;
        }


    ParticleSystem {
        anchors.fill: parent
        ImageParticle {
            groups: ["stars"]
            anchors.fill: parent
            source: "qrc:/img/star.png"
            rotationVariation: 360

        }
        Emitter {
            group: "stars"
            emitRate: 30
            lifeSpan: 5000
            size: 10
            sizeVariation: 24

            velocity: AngleDirection {angleVariation: 360; magnitude: 5}
            anchors.fill: parent
        }

        Turbulence {
            anchors.fill: parent
            strength: 8
        }
    }
}

