import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.12
import Qt3D.Input 2.0
import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt3D.Input 2.0
import QtQuick.Controls.Material 2.3


Entity {
    id: sceneRoot

    Camera {
        id: camera

        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        nearPlane: 0.1
        farPlane: 20.0
        position: Qt.vector3d(0.0,8.0,0.0)
        upVector: Qt.vector3d(0.0, 0.0, 0.0)
        viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
    }

    FirstPersonCameraController {
        camera: camera
    }


    components: [
        RenderSettings {
            activeFrameGraph: ForwardRenderer {
                id: renderer
                clearColor: "black"
                camera: camera
            }
        },
        InputSettings { }
    ]


    Entity
    {
          components: [
        ExtrudedTextMesh
        {
            id: textMesh
            text: "Starship Battle"
            FontLoader {
                    id: starFont
                    source: "qrc:/fonts/Star.otf"
                }
            font.family: starFont.name
            depth: 0.5


        },

        DiffuseSpecularMaterial {
            diffuse: "yellow"
        },

        Transform {
            id: textTransform
            property int rotY: 0
            property real distance: -20

            translation: Qt.vector3d(-18, distance, 1)
            rotation: fromAxisAndAngle(Qt.vector3d(1,0,0),rotY)

            scale: 3.0
            NumberAnimation
            {
                running: true
                loops: 1
                target: textTransform
                property: "distance"
                duration: 10000
                easing.type: Easing.InOutQuad
                from: -20
                to: -2
                onFinished:
                {
                    rotationAnim.start()
                }
            }

            SequentialAnimation
            {
                id: rotationAnim
                loops: 1
                running: false
                NumberAnimation {
                    target: textTransform
                    property: "rotY"
                    duration: 20000
                    easing.type: Easing.InOutQuad
                    from: 0
                    to: -90


                }
                onFinished:
                {
                    lightText.animationRunning = true
                }
            }
        }
          ] // TEXT ENTITY
    }
    LightsText
    {
        id: lightText
        animationRunning: false
    }
}
