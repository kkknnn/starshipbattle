#include "bluetooth.h"

Bluetooth::Bluetooth(GameController &controler) : uuid("70457ad4-9844-11ea-bb37-0242ac130002"), gameControler(controler)
{
    if (localDevice.isValid())
    {
        // Turn Bluetooth on
        localDevice.powerOn();

        // Read local device name
        QString localDeviceName = localDevice.name();
        qDebug() << "Bluetooth device ok. Name :  " << localDeviceName << '\n';
        // Make it visible to others
        localDevice.setHostMode(QBluetoothLocalDevice::HostDiscoverable);

        deviceExplorer = std::make_shared<QBluetoothDeviceDiscoveryAgent>();
        timerDebug = std::make_shared<QTimer>();
        connect(deviceExplorer.get(), SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)), this,
                SLOT(deviceFound(QBluetoothDeviceInfo)));
        timerDebug->setInterval(2000);
        connect(timerDebug.get(), SIGNAL(timeout()), this, SLOT(printDeviceNames()));

        // comm server
        commServer = std::make_shared<QBluetoothServer>(QBluetoothServiceInfo::RfcommProtocol);
        connect(commServer.get(), SIGNAL(newConnection()), this, SLOT(newConnection()));
    }
    else
    {
        qCritical() << "Bluetooth device error" << '\n';
    }
}

void Bluetooth::startDeviceSearch() const
{
    deviceExplorer->start();
    if (!timerDebug->isActive())
    {
        timerDebug->start();
    }
}

void Bluetooth::stopDeviceSearch() const
{
    deviceExplorer->stop();
    if (timerDebug->isActive())
    {
        timerDebug->stop();
    }
}

void Bluetooth::setName(const QString &newName)
{
    serverName = newName;
}

void Bluetooth::printDeviceNames()
{
    std::unique_lock<std::mutex> lck(mtx);
    qDebug() << "BLUETOOTH DEVICES" << '\n';
    for (const auto &elem : foundDeviceNames)
    {
        qDebug() << elem << '\n';
    }
}

void Bluetooth::initAdvertisingData()
{
    advertisingData.setDiscoverability(QLowEnergyAdvertisingData::DiscoverabilityGeneral);
    advertisingData.setIncludePowerLevel(true);
    advertisingData.setLocalName(serverName);
    QList<QBluetoothUuid> services;
    services << QBluetoothUuid::AV_RemoteControlController;
    advertisingData.setServices(services);
}

void Bluetooth::initServiceData()
{
    charData.setUuid(QBluetoothUuid::AV_RemoteControlController);
    charData.setValue(QByteArray(2, 0));
    charData.setProperties(QLowEnergyCharacteristic::Notify);
    const QLowEnergyDescriptorData clientConfig(QBluetoothUuid::ClientCharacteristicConfiguration, QByteArray(2, 0));
    charData.addDescriptor(clientConfig);
    serviceData.setType(QLowEnergyServiceData::ServiceTypePrimary);
    serviceData.setUuid(QBluetoothUuid::HeartRate);
    serviceData.addCharacteristic(charData);
}

QString Bluetooth::getServerName() const
{
    return serverName;
}

void Bluetooth::startListen()
{

    const QBluetoothAddress localAdapter = QBluetoothAddress();

    if (!commServer->listen(localAdapter))
    {
        qWarning() << "Cannot bind chat server to" << localAdapter.toString();
        return;
    }
    QBluetoothServiceInfo::Sequence profileSequence;
    QBluetoothServiceInfo::Sequence classId;
    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));
    classId << QVariant::fromValue(quint16(0x100));
    profileSequence.append(QVariant::fromValue(classId));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BluetoothProfileDescriptorList, profileSequence);

    classId.clear();
    classId << QVariant::fromValue(QBluetoothUuid(uuid));
    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceClassIds, classId);

    //! [Service name, description and provider]
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceName, tr("Bt Chat Server"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceDescription, tr("Example bluetooth chat server"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceProvider, tr("qt-project.org"));
    //! [Service name, description and provider]

    //! [Service UUID set]
    serviceInfo.setServiceUuid(QBluetoothUuid(uuid));
    //! [Service UUID set]

    //! [Service Discoverability]
    QBluetoothServiceInfo::Sequence publicBrowse;
    publicBrowse << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::PublicBrowseGroup));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BrowseGroupList, publicBrowse);
    //! [Service Discoverability]

    //! [Protocol descriptor list]
    QBluetoothServiceInfo::Sequence protocolDescriptorList;
    QBluetoothServiceInfo::Sequence protocol;
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::L2cap));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    protocol.clear();
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::Rfcomm))
             << QVariant::fromValue(quint8(commServer->serverPort()));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ProtocolDescriptorList, protocolDescriptorList);
    //! [Protocol descriptor list]

    //! [Register service]
    serviceInfo.registerService(localAdapter);
    //! [Register service]
}

void Bluetooth::stopListen()
{
}

void Bluetooth::deviceFound(const QBluetoothDeviceInfo &device)
{
    if (device.isValid())
    {
        foundDeviceNames.push_back(device.name());
        device.serviceClasses();
    }
}

void Bluetooth::newConnection()
{
    socket = commServer->nextPendingConnection();
    qDebug() << socket->localName() << socket->localAddress() << '\n';

    if (!socket)
    {
        qDebug() << "comm server bluetooth socket error" << '\n';
        return;
    }

    connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    clientSockets.append(socket);
}

void Bluetooth::clientDisconnected()
{
    qDebug() << "connection lost !!!!!!!!" << '\n';
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)
        return;

    emit clientDisconnectedSig(socket->peerName());

    clientSockets.removeOne(socket);

    socket->deleteLater();
}

void Bluetooth::readSocket()
{
    qInfo() << "Bluetooth packet received";

    QByteArray packet = socket->readLine();
    if (packet.size() == sizeof(GameController::ControlsKeys))
    {
        gameControler.procedKey(static_cast<GameController::ControlsKeys>(*packet.data()));
    }
    else
    {
        qDebug() << "Packet with wrong size received" << '\n';
    }

    while (socket->canReadLine())
    {
        qDebug() << "readline" << '\n';
        QByteArray message = socket->readLine().trimmed();
        qDebug() << message << "\n";
        emit messageReceivedViaBluetooth(socket->peerName(), message);
    }
}
